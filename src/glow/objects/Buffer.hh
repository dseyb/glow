#pragma once

#include "glow/common/shared.hh"
#include "glow/common/non_copyable.hh"

#include "glow/gl.hh"

namespace glow
{
GLOW_SHARED(class, Buffer);

class Buffer
{
    GLOW_NON_COPYABLE(Buffer);

    /// OGL id
    GLuint mObjectName;

    /// Buffer type
    GLenum mType;

public: // getter
    GLuint getObjectName() const { return mObjectName; }
    GLenum getType() const { return mType; }
protected:
    Buffer(GLenum bufferType);
    virtual ~Buffer();
};
}
