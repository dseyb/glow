// This file is auto-generated and should not be modified directly.
#include "Texture3D.hh"

#include <glm/glm.hpp>

#include "glow/data/SurfaceData.hh"
#include "glow/data/TextureData.hh"

#include "glow/glow.hh"
#include "glow/limits.hh"
#include "glow/common/runtime_assert.hh"
#include "glow/common/ogl_typeinfo.hh"
#include "glow/common/scoped_gl.hh"

using namespace glow;

Texture3D::BoundTexture3D *Texture3D::sCurrentTexture = nullptr;

Texture3D::BoundTexture3D *Texture3D::getCurrentTexture()
{
    return sCurrentTexture;
}

bool Texture3D::hasMipmapsEnabled() const
{
    switch (mMinFilter)
    {
    case GL_LINEAR_MIPMAP_LINEAR:
    case GL_LINEAR_MIPMAP_NEAREST:
    case GL_NEAREST_MIPMAP_LINEAR:
    case GL_NEAREST_MIPMAP_NEAREST:
        return true;

    default:
        return false;
    }
}

Texture3D::Texture3D(GLenum internalFormat) : Texture(GL_TEXTURE_3D, GL_TEXTURE_BINDING_3D, internalFormat)
{
}

SharedTexture3D Texture3D::create(size_t width, size_t height, size_t depth, GLenum internalFormat)
{
    auto tex = std::make_shared<Texture3D>(internalFormat);
    tex->bind().resize(width, height, depth);
    return tex;
}

SharedTexture3D Texture3D::createStorageImmutable(size_t width, size_t height, size_t depth, GLenum internalFormat, int mipmapLevels)
{
    auto tex = std::make_shared<Texture3D>(internalFormat);
    tex->bind().makeStorageImmutable(width, height, depth, internalFormat, mipmapLevels);
    return tex;
}

SharedTexture3D Texture3D::createFromFile(const std::string &filename, ColorSpace colorSpace)
{
    return createFromData(TextureData::createFromFile(filename, colorSpace));
}

SharedTexture3D Texture3D::createFromFile(const std::string &filename, GLenum internalFormat, ColorSpace colorSpace)
{
    return createFromData(TextureData::createFromFile(filename, colorSpace), internalFormat);
}

SharedTexture3D Texture3D::createFromData(const SharedTextureData &data)
{
    if (!data)
    {
        error() << "Texture3D::createFromData failed, no data provided";
        return nullptr;
    }

    if (data->getPreferredInternalFormat() == GL_INVALID_ENUM)
    {
        error() << "Texture3D::createFromData failed, no preferred internal format specified";
        return nullptr;
    }

    auto tex = std::make_shared<Texture3D>(data->getPreferredInternalFormat());
    tex->bind().setData(data->getPreferredInternalFormat(), data);
    return tex;
}

SharedTexture3D Texture3D::createFromData(const SharedTextureData &data, GLenum internalFormat)
{
    if (!data)
    {
        error() << "Texture3D::createFromData failed, no data provided";
        return nullptr;
    }

    auto tex = std::make_shared<Texture3D>(internalFormat);
    tex->bind().setData(internalFormat, data);
    return tex;
}

bool Texture3D::BoundTexture3D::isCurrent() const
{
    GLOW_RUNTIME_ASSERT(sCurrentTexture == this, "Currently bound FBO does NOT match represented Texture", return false);
    return true;
}

void Texture3D::BoundTexture3D::makeStorageImmutable(size_t width, size_t height, size_t depth, GLenum internalFormat, int mipmapLevels)
{
    if (!isCurrent())
        return;

    GLOW_RUNTIME_ASSERT(!texture->isStorageImmutable(), "Texture is already immutable", return );

    if (mipmapLevels <= 0)
        mipmapLevels = glm::floor(glm::log2(glm::max((float)width, glm::max((float)height, (float)depth)))) + 1;

    texture->mStorageImmutable = true;
    texture->mInternalFormat = internalFormat;
    texture->mWidth = width;
    texture->mHeight = height;
    texture->mDepth = depth;
    glTexStorage3D(texture->mTarget, mipmapLevels, internalFormat, width, height, depth);
}

void Texture3D::BoundTexture3D::setMinFilter(GLenum filter)
{
    if (!isCurrent())
        return;


    glTexParameteri(texture->mTarget, GL_TEXTURE_MIN_FILTER, filter);
    texture->mMinFilter = filter;
}

void Texture3D::BoundTexture3D::setMagFilter(GLenum filter)
{
    if (!isCurrent())
        return;

    glTexParameteri(texture->mTarget, GL_TEXTURE_MAG_FILTER, filter);
    texture->mMagFilter = filter;
}

void Texture3D::BoundTexture3D::setFilter(GLenum magFilter, GLenum minFilter)
{
    if (!isCurrent())
        return;

    glTexParameteri(texture->mTarget, GL_TEXTURE_MIN_FILTER, minFilter);
    glTexParameteri(texture->mTarget, GL_TEXTURE_MAG_FILTER, magFilter);
    texture->mMinFilter = minFilter;
    texture->mMagFilter = magFilter;
}

void Texture3D::BoundTexture3D::setAnisotropicFiltering(GLfloat samples)
{
    if (!isCurrent())
        return;

    samples = glm::clamp(samples, 1.f, limits::maxAnisotropy);
    glTexParameterf(texture->mTarget, GL_TEXTURE_MAX_ANISOTROPY_EXT, samples);
    texture->mAnisotropicFiltering = samples;
}

void Texture3D::BoundTexture3D::setWrapS(GLenum wrap)
{
    if (!isCurrent())
        return;

    glTexParameteri(texture->mTarget, GL_TEXTURE_WRAP_S, wrap);
    texture->mWrapS = wrap;
}
void Texture3D::BoundTexture3D::setWrapT(GLenum wrap)
{
    if (!isCurrent())
        return;

    glTexParameteri(texture->mTarget, GL_TEXTURE_WRAP_T, wrap);
    texture->mWrapT = wrap;
}
void Texture3D::BoundTexture3D::setWrapR(GLenum wrap)
{
    if (!isCurrent())
        return;

    glTexParameteri(texture->mTarget, GL_TEXTURE_WRAP_R, wrap);
    texture->mWrapR = wrap;
}

void Texture3D::BoundTexture3D::setWrap(GLenum wrapS, GLenum wrapT, GLenum wrapR)
{
    if (!isCurrent())
        return;

    glTexParameteri(texture->mTarget, GL_TEXTURE_WRAP_S, wrapS);
    texture->mWrapS = wrapS;
    glTexParameteri(texture->mTarget, GL_TEXTURE_WRAP_T, wrapT);
    texture->mWrapT = wrapT;
    glTexParameteri(texture->mTarget, GL_TEXTURE_WRAP_R, wrapR);
    texture->mWrapR = wrapR;
}

void Texture3D::BoundTexture3D::generateMipmaps()
{
    if (!isCurrent())
        return;

    glGenerateMipmap(texture->mTarget);
    texture->mMipmapsGenerated = true;
}

void Texture3D::BoundTexture3D::resize(size_t width, size_t height, size_t depth)
{
    if (!isCurrent())
        return;

    GLOW_RUNTIME_ASSERT(!texture->isStorageImmutable(), "Texture is storage immutable", return );

    texture->mWidth = width;
    texture->mHeight = height;
    texture->mDepth = depth;

    GLenum format = GL_RGBA;
    switch (texture->mInternalFormat)
    {
    case GL_DEPTH_COMPONENT:
    case GL_DEPTH_COMPONENT16:
    case GL_DEPTH_COMPONENT24:
    case GL_DEPTH_COMPONENT32:
    case GL_DEPTH_COMPONENT32F:
        format = GL_DEPTH_COMPONENT;
        break;
    }

    glTexImage3D(texture->mTarget, 0, texture->mInternalFormat, width, height, depth, 0, format, GL_FLOAT, nullptr);
}

void Texture3D::BoundTexture3D::setData(
    GLenum internalFormat, size_t width, size_t height, size_t depth, GLenum format, GLenum type, const GLvoid *data, int mipmapLevel)
{
    if (!isCurrent())
        return;

    if (texture->isStorageImmutable())
    {
        assert(mipmapLevel == 0 && "not implemented for higher levels");
        GLOW_RUNTIME_ASSERT(texture->mWidth == width, "Texture is storage immutable and a wrong width was specified", return );
        GLOW_RUNTIME_ASSERT(texture->mHeight == height, "Texture is storage immutable and a wrong height was specified", return );
        GLOW_RUNTIME_ASSERT(texture->mDepth == depth, "Texture is storage immutable and a wrong depth was specified", return );
        GLOW_RUNTIME_ASSERT(texture->mInternalFormat == internalFormat,
                            "Texture is storage immutable and a wrong internal format was specified", return );
    }

    if (mipmapLevel == 0)
    {
        texture->mWidth = width;
        texture->mHeight = height;
        texture->mDepth = depth;
        texture->mMipmapsGenerated = false;
        texture->mInternalFormat = internalFormat;
    }

    // assure proper pixel store parameter
    scoped::unpackSwapBytes _p0(false);
    scoped::unpackLsbFirst _p1(false);
    scoped::unpackRowLength _p2(0);
    scoped::unpackImageHeight _p3(0);
    scoped::unpackSkipRows _p4(0);
    scoped::unpackSkipPixels _p5(0);
    scoped::unpackSkipImages _p6(0);
    scoped::unpackAlignment _p7(1); // tight

    glTexImage3D(texture->mTarget, mipmapLevel, texture->mInternalFormat, width, height, depth, 0, format, type, data);
}

void Texture3D::BoundTexture3D::setData(GLenum internalFormat, const SharedTextureData &data)
{
    if (!isCurrent())
        return;

    texture->mInternalFormat = internalFormat; // format first, then resize
    resize(data->getWidth(), data->getHeight(), data->getDepth());

    // set all level 0 surfaces
    for (auto const &surf : data->getSurfaces())
        if (surf->getMipmapLevel() == 0)
            setData(internalFormat, surf->getWidth(), surf->getHeight(), surf->getDepth(), surf->getFormat(),
                    surf->getType(), surf->getData().data(), surf->getMipmapLevel());

    // set parameters
    if (data->getAnisotropicFiltering() >= 1.f)
        setAnisotropicFiltering(data->getAnisotropicFiltering());
    if (data->getMinFilter() != GL_INVALID_ENUM)
        setMinFilter(data->getMinFilter());
    if (data->getMagFilter() != GL_INVALID_ENUM)
        setMagFilter(data->getMagFilter());
    if (data->getWrapS() != GL_INVALID_ENUM)
        setWrapS(data->getWrapS());
    if (data->getWrapT() != GL_INVALID_ENUM)
        setWrapT(data->getWrapT());
    if (data->getWrapR() != GL_INVALID_ENUM)
        setWrapR(data->getWrapR());

    // generate mipmaps
    if (texture->hasMipmapsEnabled())
        generateMipmaps();

    // set all level 1+ surfaces
    for (auto const &surf : data->getSurfaces())
        if (surf->getMipmapLevel() > 0)
            setData(internalFormat, surf->getWidth(), surf->getHeight(), surf->getDepth(), surf->getFormat(),
                    surf->getType(), surf->getData().data(), surf->getMipmapLevel());
}

std::vector<char> Texture3D::BoundTexture3D::getData(GLenum format, GLenum type, int mipmapLevel)
{
    if (!isCurrent())
        return {};

    if (mipmapLevel != 0)
    {
        error() << "not implemented";
        return {};
    }

    // assure proper pixel store parameter
    scoped::packSwapBytes _p0(false);
    scoped::packLsbFirst _p1(false);
    scoped::packRowLength _p2(0);
    scoped::packImageHeight _p3(0);
    scoped::packSkipRows _p4(0);
    scoped::packSkipPixels _p5(0);
    scoped::packSkipImages _p6(0);
    scoped::packAlignment _p7(1); // tight

    std::vector<char> data;
    data.resize(texture->mWidth * texture->mHeight * texture->mDepth * channelsOfFormat(format) * sizeOfTypeInBytes(type));
    glGetTexImage(texture->mTarget, mipmapLevel, format, type, data.data());
    return data;
}

void Texture3D::BoundTexture3D::getData(GLenum format, GLenum type, size_t bufferSize, void *buffer, int mipmapLevel)
{
    if (!isCurrent())
        return;

    if (mipmapLevel != 0)
    {
        error() << "not implemented";
        return;
    }

    // assure proper pixel store parameter
    scoped::packSwapBytes _p0(false);
    scoped::packLsbFirst _p1(false);
    scoped::packRowLength _p2(0);
    scoped::packImageHeight _p3(0);
    scoped::packSkipRows _p4(0);
    scoped::packSkipPixels _p5(0);
    scoped::packSkipImages _p6(0);
    scoped::packAlignment _p7(1); // tight

    (void)bufferSize; // TODO: check me!
    glGetTexImage(texture->mTarget, mipmapLevel, format, type, buffer);
}

SharedTextureData Texture3D::BoundTexture3D::getTextureData(int maxMipLevel)
{
    if (!isCurrent())
        return nullptr;

    // assure proper pixel store parameter
    scoped::packSwapBytes _p0(false);
    scoped::packLsbFirst _p1(false);
    scoped::packRowLength _p2(0);
    scoped::packImageHeight _p3(0);
    scoped::packSkipRows _p4(0);
    scoped::packSkipPixels _p5(0);
    scoped::packSkipImages _p6(0);
    scoped::packAlignment _p7(1); // tight

    auto tex = std::make_shared<TextureData>();

    // tex parameters
    // TODO

    // surfaces

    for (auto lvl = 0; lvl < maxMipLevel; ++lvl)
    {
        GLint w;
        GLint h;
        GLint d;
        GLenum internalFormat;
        glGetTexLevelParameteriv(texture->mTarget, lvl, GL_TEXTURE_WIDTH, &w);
        glGetTexLevelParameteriv(texture->mTarget, lvl, GL_TEXTURE_HEIGHT, &h);
        glGetTexLevelParameteriv(texture->mTarget, lvl, GL_TEXTURE_DEPTH, &d);
        glGetTexLevelParameteriv(texture->mTarget, lvl, GL_TEXTURE_INTERNAL_FORMAT, (GLint *)&internalFormat);


        if (w * h * d == 0)
            break; // no mipmaps any more

        if (lvl == 0)
        {
            tex->setWidth(w);
            tex->setHeight(h);
            tex->setDepth(d);
            tex->setPreferredInternalFormat(internalFormat);
        }

        auto surface = std::make_shared<SurfaceData>();
        surface->setWidth(w);
        surface->setHeight(h);
        surface->setDepth(d);

        // TODO: support all formats
        surface->setFormat(GL_RGBA);
        surface->setType(GL_UNSIGNED_BYTE);

        std::vector<char> data;
        data.resize(4 * w * h * d);
        glGetTexImage(texture->mTarget, lvl, surface->getFormat(), surface->getType(), data.data());
        surface->setData(data);

        tex->addSurface(surface);
    }

    return tex;
}

void Texture3D::BoundTexture3D::writeToFile(const std::string &filename)
{
    getTextureData()->saveToFile(filename);
}


Texture3D::BoundTexture3D::BoundTexture3D(Texture3D *texture) : texture(texture)
{
    glGetIntegerv(texture->mBindingTarget, &previousTexture);
    glActiveTexture(GL_TEXTURE0 + limits::maxCombinedTextureImageUnits - 1);
    glBindTexture(texture->mTarget, texture->mObjectName);

    previousTexturePtr = sCurrentTexture;
    sCurrentTexture = this;
}

Texture3D::BoundTexture3D::BoundTexture3D(Texture3D::BoundTexture3D &&rhs)
  : texture(rhs.texture), previousTexture(rhs.previousTexture), previousTexturePtr(rhs.previousTexturePtr)
{
    // invalidate rhs
    rhs.previousTexture = -1;
}

Texture3D::BoundTexture3D::~BoundTexture3D()
{
    if (previousTexture != -1) // if valid
    {
        glActiveTexture(GL_TEXTURE0 + limits::maxCombinedTextureImageUnits - 1);
        glBindTexture(texture->mTarget, previousTexture);
        sCurrentTexture = previousTexturePtr;
    }
}
