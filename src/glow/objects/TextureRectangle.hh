// This file is auto-generated and should not be modified directly.
#pragma once

#include "Texture.hh"

#include "glow/common/gltypeinfo.hh"
#include "glow/common/warn_unused.hh"
#include "glow/common/log.hh"

#include "glow/data/ColorSpace.hh"

#include <vector>

namespace glow
{
GLOW_SHARED(class, TextureRectangle);
GLOW_SHARED(class, TextureData);

/// Defines a rectangular texture in OpenGL
class TextureRectangle final : public Texture
{
public:
    struct BoundTextureRectangle;

private:
    /// Currently bound texture
    static BoundTextureRectangle* sCurrentTexture;

    /// Minification filter
    GLenum mMinFilter = GL_LINEAR;
    /// Magnification filter
    GLenum mMagFilter = GL_LINEAR;

    /// Wrapping in S
    GLenum mWrapS = GL_CLAMP_TO_EDGE;
    /// Wrapping in T
    GLenum mWrapT = GL_CLAMP_TO_EDGE;

    /// Level of anisotropic filtering (>= 1.f, which is isotropic)
    /// Max number of samples basically
    GLfloat mAnisotropicFiltering = 1.0f;

    /// Texture size: Width
    size_t mWidth = 0u;
    /// Texture size: Height
    size_t mHeight = 0u;


    /// if true, this texture got immutable storage by glTexStorage2D
    bool mStorageImmutable = false;

public: // getter
    /// Gets the currently bound texture (nullptr if none)
    static BoundTextureRectangle* getCurrentTexture();

    GLenum getMinFilter() const { return mMinFilter; }
    GLenum getMagFilter() const { return mMagFilter; }
    GLenum getWrapS() const { return mWrapS; }
    GLenum getWrapT() const { return mWrapT; }
    size_t getWidth() const { return mWidth; }
    size_t getHeight() const { return mHeight; }
    bool isStorageImmutable() const override { return mStorageImmutable; }
    glm::uvec3 getDim() const override { return{ mWidth, mHeight, 0 }; }
public:
    /// RAII-object that defines a "bind"-scope for a rectangular texture
    /// All functions that operate on the currently bound tex are accessed here
    struct BoundTextureRectangle
    {
        GLOW_RAII_CLASS(BoundTextureRectangle);

        /// Backreference to the texture
        TextureRectangle* const texture;

        /// Makes the storage of this texture immutable
        /// It is an error to call this more than once
        /// It is an error to upload data with a different internal format at a later point
        /// It is an error to resize after storage was made immutable (unless it's the same size)
        /// Invalidates previously uploaded data
        void makeStorageImmutable(size_t width, size_t height, GLenum internalFormat);

        /// Sets minification filter (GL_NEAREST, GL_LINEAR)
        void setMinFilter(GLenum filter);
        /// Sets magnification filter (GL_NEAREST, GL_LINEAR)
        void setMagFilter(GLenum filter);
        /// Sets mag and min filter
        void setFilter(GLenum magFilter, GLenum minFilter);

        /// Sets the number of anisotropic samples (>= 1)
        void setAnisotropicFiltering(GLfloat samples);

        /// Sets texture wrapping in S
        void setWrapS(GLenum wrap);
        /// Sets texture wrapping in T
        void setWrapT(GLenum wrap);
        /// Sets texture wrapping in all directions
        void setWrap(GLenum wrapS, GLenum wrapT);


        /// Resizes the texture
        /// invalidates the data
        void resize(size_t width, size_t height);

        /// Generic data uploads
        /// Changes internal format, width, height, and data
        void setData(GLenum internalFormat, size_t width, size_t height, GLenum format, GLenum type, const GLvoid* data);
        /// Data upload via glm or c++ type (see gltypeinfo)
        template <typename DataT>
        void setData(GLenum internalFormat, size_t width, size_t height, std::vector<DataT> const& data)
        {
            if (data.size() != width * height)
            {
                error() << "Texture size is " << width << " x " << height << " = " << width * height << " but "
                        << data.size() << " pixels are provided.";
                return;
            }
            setData(internalFormat, width, height, glTypeOf<DataT>::format, glTypeOf<DataT>::type, data.data());
        }
        /// Same as above
        /// Usage:
        ///   glm::vec3 texData[] = { ... }
        ///   setData(texData);
        template <typename DataT, std::size_t N>
        void setData(GLenum internalFormat, size_t width, size_t height, const DataT(&data)[N])
        {
            if (N != width * height)
            {
                error() << "Texture size is " << width << " x " << height << " = " << width * height << " but " << N
                        << " pixels are provided.";
                return;
            }
            setData(internalFormat, width, height, glTypeOf<DataT>::format, glTypeOf<DataT>::type, data);
        }
        /// Same as above
        /// Usage:
        ///   glm::vec3 texData[][] = { ... }
        ///   // it's [height][width]
        ///   setData(texData);
        template <typename DataT, size_t width, size_t height>
        void setData(GLenum internalFormat, const DataT(&data)[height][width])
        {
            setData(internalFormat, width, height, glTypeOf<DataT>::format, glTypeOf<DataT>::type, data);
        }

        /// Sets texture data from surface data
        /// May set multiple levels at once
        /// May modify texture parameter
        void setData(GLenum internalFormat, SharedTextureData const& data);

        /// Generic data download
        std::vector<char> getData(GLenum format, GLenum type);
        /// Generic data download
        void getData(GLenum format, GLenum type, size_t bufferSize, void* buffer);
        /// Data upload via glm or c++ type (see gltypeinfo)
        template <typename DataT>
        std::vector<DataT> getData(int mipmapLevel = 0)
        {
            std::vector<DataT> data;
            data.resize(texture->mWidth * texture->mHeight);
            getData(glTypeOf<DataT>::format, glTypeOf<DataT>::type, data.size() * sizeof(DataT), data.data());
            return std::move(data);
        }

        /// Extracts all stored surface data
        /// This is useful for saving the texture to a file
        SharedTextureData getTextureData();
        /// Same as getTextureData()->writeToFile(filename)
        void writeToFile(std::string const& filename);

    private:
        GLint previousTexture;                     ///< previously bound tex
        BoundTextureRectangle* previousTexturePtr; ///< previously bound tex
        BoundTextureRectangle(TextureRectangle* buffer);
        friend class TextureRectangle;

        /// returns true iff it's safe to use this bound class
        /// otherwise, runtime error
        bool isCurrent() const;

    public:
        BoundTextureRectangle(BoundTextureRectangle&&); // allow move
        ~BoundTextureRectangle();
    };

public:
    TextureRectangle(GLenum internalFormat = GL_RGBA);

    /// Binds this texture.
    /// Unbinding is done when the returned object runs out of scope.
    GLOW_WARN_UNUSED BoundTextureRectangle bind() { return {this}; }
public: // static construction
    /// Creates a rectangular texture with given width and height
    static SharedTextureRectangle create(size_t width = 1, size_t height = 1, GLenum internalFormat = GL_RGBA);
    /// Creates a rectangular texture with given width and height which is storage immutable
    static SharedTextureRectangle createStorageImmutable(size_t width, size_t height, GLenum internalFormat);

    /// Creates a rectangular texture from file
    /// See TextureData::createFromFile for format documentation
    /// Uses preferred internal format
    static SharedTextureRectangle createFromFile(std::string const& filename, ColorSpace colorSpace = ColorSpace::AutoDetect);
    /// same as createFromFile but with custom internal format
    static SharedTextureRectangle createFromFile(std::string const& filename,
                                                 GLenum internalFormat,
                                                 ColorSpace colorSpace = ColorSpace::AutoDetect);

    /// Creates a rectangular texture from given data
    /// Uses preferred internal format
    static SharedTextureRectangle createFromData(SharedTextureData const& data);
    /// same as createFromData but with custom internal format
    static SharedTextureRectangle createFromData(SharedTextureData const& data, GLenum internalFormat);
};
}
