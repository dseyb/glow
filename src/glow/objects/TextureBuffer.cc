// This file is auto-generated and should not be modified directly.
#include "TextureBuffer.hh"

#include <glm/glm.hpp>

#include "glow/data/SurfaceData.hh"
#include "glow/data/TextureData.hh"

#include "glow/glow.hh"
#include "glow/limits.hh"
#include "glow/common/runtime_assert.hh"
#include "glow/common/ogl_typeinfo.hh"
#include "glow/common/scoped_gl.hh"

using namespace glow;

TextureBuffer::BoundTextureBuffer *TextureBuffer::sCurrentTexture = nullptr;

TextureBuffer::BoundTextureBuffer *TextureBuffer::getCurrentTexture()
{
    return sCurrentTexture;
}


TextureBuffer::TextureBuffer(GLenum internalFormat)
  : Texture(GL_TEXTURE_BUFFER, GL_TEXTURE_BINDING_BUFFER, internalFormat)
{
}

SharedTextureBuffer TextureBuffer::create(size_t size, GLenum internalFormat)
{
    auto tex = std::make_shared<TextureBuffer>(internalFormat);
    tex->bind().resize(size);
    return tex;
}


SharedTextureBuffer TextureBuffer::createFromFile(const std::string &filename, ColorSpace colorSpace)
{
    return createFromData(TextureData::createFromFile(filename, colorSpace));
}

SharedTextureBuffer TextureBuffer::createFromFile(const std::string &filename, GLenum internalFormat, ColorSpace colorSpace)
{
    return createFromData(TextureData::createFromFile(filename, colorSpace), internalFormat);
}

SharedTextureBuffer TextureBuffer::createFromData(const SharedTextureData &data)
{
    if (!data)
    {
        error() << "TextureBuffer::createFromData failed, no data provided";
        return nullptr;
    }

    if (data->getPreferredInternalFormat() == GL_INVALID_ENUM)
    {
        error() << "TextureBuffer::createFromData failed, no preferred internal format specified";
        return nullptr;
    }

    auto tex = std::make_shared<TextureBuffer>(data->getPreferredInternalFormat());
    tex->bind().setData(data->getPreferredInternalFormat(), data);
    return tex;
}

SharedTextureBuffer TextureBuffer::createFromData(const SharedTextureData &data, GLenum internalFormat)
{
    if (!data)
    {
        error() << "TextureBuffer::createFromData failed, no data provided";
        return nullptr;
    }

    auto tex = std::make_shared<TextureBuffer>(internalFormat);
    tex->bind().setData(internalFormat, data);
    return tex;
}

bool TextureBuffer::BoundTextureBuffer::isCurrent() const
{
    GLOW_RUNTIME_ASSERT(sCurrentTexture == this, "Currently bound FBO does NOT match represented Texture", return false);
    return true;
}


void TextureBuffer::BoundTextureBuffer::resize(size_t size)
{
    if (!isCurrent())
        return;


    texture->mSize = size;


    warning() << "not implemented.";
}

void TextureBuffer::BoundTextureBuffer::setData(GLenum internalFormat, size_t size, GLenum format, GLenum type, const GLvoid *data)
{
    if (!isCurrent())
        return;


    texture->mSize = size;
    texture->mInternalFormat = internalFormat;

    // assure proper pixel store parameter
    scoped::unpackSwapBytes _p0(false);
    scoped::unpackLsbFirst _p1(false);
    scoped::unpackRowLength _p2(0);
    scoped::unpackImageHeight _p3(0);
    scoped::unpackSkipRows _p4(0);
    scoped::unpackSkipPixels _p5(0);
    scoped::unpackSkipImages _p6(0);
    scoped::unpackAlignment _p7(1); // tight

    glTexImage1D(texture->mTarget, 0, texture->mInternalFormat, size, 0, format, type, data);
}

void TextureBuffer::BoundTextureBuffer::setData(GLenum internalFormat, const SharedTextureData &data)
{
    if (!isCurrent())
        return;

    texture->mInternalFormat = internalFormat; // format first, then resize
    resize(data->getWidth());

    // set all level 0 surfaces
    for (auto const &surf : data->getSurfaces())
        if (surf->getMipmapLevel() == 0)
            setData(internalFormat, surf->getWidth(), surf->getFormat(), surf->getType(), surf->getData().data());
}

std::vector<char> TextureBuffer::BoundTextureBuffer::getData(GLenum format, GLenum type)
{
    if (!isCurrent())
        return {};


    // assure proper pixel store parameter
    scoped::packSwapBytes _p0(false);
    scoped::packLsbFirst _p1(false);
    scoped::packRowLength _p2(0);
    scoped::packImageHeight _p3(0);
    scoped::packSkipRows _p4(0);
    scoped::packSkipPixels _p5(0);
    scoped::packSkipImages _p6(0);
    scoped::packAlignment _p7(1); // tight

    std::vector<char> data;
    data.resize(texture->mSize * channelsOfFormat(format) * sizeOfTypeInBytes(type));
    glGetTexImage(texture->mTarget, 0, format, type, data.data());
    return data;
}

void TextureBuffer::BoundTextureBuffer::getData(GLenum format, GLenum type, size_t bufferSize, void *buffer)
{
    if (!isCurrent())
        return;


    // assure proper pixel store parameter
    scoped::packSwapBytes _p0(false);
    scoped::packLsbFirst _p1(false);
    scoped::packRowLength _p2(0);
    scoped::packImageHeight _p3(0);
    scoped::packSkipRows _p4(0);
    scoped::packSkipPixels _p5(0);
    scoped::packSkipImages _p6(0);
    scoped::packAlignment _p7(1); // tight

    (void)bufferSize; // TODO: check me!
    glGetTexImage(texture->mTarget, 0, format, type, buffer);
}

SharedTextureData TextureBuffer::BoundTextureBuffer::getTextureData()
{
    if (!isCurrent())
        return nullptr;

    // assure proper pixel store parameter
    scoped::packSwapBytes _p0(false);
    scoped::packLsbFirst _p1(false);
    scoped::packRowLength _p2(0);
    scoped::packImageHeight _p3(0);
    scoped::packSkipRows _p4(0);
    scoped::packSkipPixels _p5(0);
    scoped::packSkipImages _p6(0);
    scoped::packAlignment _p7(1); // tight

    auto tex = std::make_shared<TextureData>();

    // tex parameters
    // TODO

    // surfaces
    {
        auto lvl = 0;
        GLint w;
        GLint h;
        GLint d;
        GLenum internalFormat;
        glGetTexLevelParameteriv(texture->mTarget, lvl, GL_TEXTURE_WIDTH, &w);
        glGetTexLevelParameteriv(texture->mTarget, lvl, GL_TEXTURE_HEIGHT, &h);
        glGetTexLevelParameteriv(texture->mTarget, lvl, GL_TEXTURE_DEPTH, &d);
        glGetTexLevelParameteriv(texture->mTarget, lvl, GL_TEXTURE_INTERNAL_FORMAT, (GLint *)&internalFormat);

        {
            tex->setWidth(w);
            tex->setHeight(h);
            tex->setDepth(d);
            tex->setPreferredInternalFormat(internalFormat);
        }

        auto surface = std::make_shared<SurfaceData>();
        surface->setWidth(w);
        surface->setHeight(h);
        surface->setDepth(d);

        // TODO: support all formats
        surface->setFormat(GL_RGBA);
        surface->setType(GL_UNSIGNED_BYTE);

        std::vector<char> data;
        data.resize(4 * w * h * d);
        glGetTexImage(texture->mTarget, lvl, surface->getFormat(), surface->getType(), data.data());
        surface->setData(data);

        tex->addSurface(surface);
    }

    return tex;
}

void TextureBuffer::BoundTextureBuffer::writeToFile(const std::string &filename)
{
    getTextureData()->saveToFile(filename);
}


TextureBuffer::BoundTextureBuffer::BoundTextureBuffer(TextureBuffer *texture) : texture(texture)
{
    glGetIntegerv(texture->mBindingTarget, &previousTexture);
    glActiveTexture(GL_TEXTURE0 + limits::maxCombinedTextureImageUnits - 1);
    glBindTexture(texture->mTarget, texture->mObjectName);

    previousTexturePtr = sCurrentTexture;
    sCurrentTexture = this;
}

TextureBuffer::BoundTextureBuffer::BoundTextureBuffer(TextureBuffer::BoundTextureBuffer &&rhs)
  : texture(rhs.texture), previousTexture(rhs.previousTexture), previousTexturePtr(rhs.previousTexturePtr)
{
    // invalidate rhs
    rhs.previousTexture = -1;
}

TextureBuffer::BoundTextureBuffer::~BoundTextureBuffer()
{
    if (previousTexture != -1) // if valid
    {
        glActiveTexture(GL_TEXTURE0 + limits::maxCombinedTextureImageUnits - 1);
        glBindTexture(texture->mTarget, previousTexture);
        sCurrentTexture = previousTexturePtr;
    }
}
