#include "Buffer.hh"

using namespace glow;

Buffer::Buffer(GLenum bufferType) : mType(bufferType)
{
    glGenBuffers(1, &mObjectName);
}

Buffer::~Buffer()
{
    glDeleteBuffers(1, &mObjectName);
}
