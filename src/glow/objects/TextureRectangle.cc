// This file is auto-generated and should not be modified directly.
#include "TextureRectangle.hh"

#include <glm/glm.hpp>

#include "glow/data/SurfaceData.hh"
#include "glow/data/TextureData.hh"

#include "glow/glow.hh"
#include "glow/limits.hh"
#include "glow/common/runtime_assert.hh"
#include "glow/common/ogl_typeinfo.hh"
#include "glow/common/scoped_gl.hh"

using namespace glow;

TextureRectangle::BoundTextureRectangle *TextureRectangle::sCurrentTexture = nullptr;

TextureRectangle::BoundTextureRectangle *TextureRectangle::getCurrentTexture()
{
    return sCurrentTexture;
}


TextureRectangle::TextureRectangle(GLenum internalFormat)
  : Texture(GL_TEXTURE_RECTANGLE, GL_TEXTURE_BINDING_RECTANGLE, internalFormat)
{
}

SharedTextureRectangle TextureRectangle::create(size_t width, size_t height, GLenum internalFormat)
{
    auto tex = std::make_shared<TextureRectangle>(internalFormat);
    tex->bind().resize(width, height);
    return tex;
}

SharedTextureRectangle TextureRectangle::createStorageImmutable(size_t width, size_t height, GLenum internalFormat)
{
    auto tex = std::make_shared<TextureRectangle>(internalFormat);
    tex->bind().makeStorageImmutable(width, height, internalFormat);
    return tex;
}

SharedTextureRectangle TextureRectangle::createFromFile(const std::string &filename, ColorSpace colorSpace)
{
    return createFromData(TextureData::createFromFile(filename, colorSpace));
}

SharedTextureRectangle TextureRectangle::createFromFile(const std::string &filename, GLenum internalFormat, ColorSpace colorSpace)
{
    return createFromData(TextureData::createFromFile(filename, colorSpace), internalFormat);
}

SharedTextureRectangle TextureRectangle::createFromData(const SharedTextureData &data)
{
    if (!data)
    {
        error() << "TextureRectangle::createFromData failed, no data provided";
        return nullptr;
    }

    if (data->getPreferredInternalFormat() == GL_INVALID_ENUM)
    {
        error() << "TextureRectangle::createFromData failed, no preferred internal format specified";
        return nullptr;
    }

    auto tex = std::make_shared<TextureRectangle>(data->getPreferredInternalFormat());
    tex->bind().setData(data->getPreferredInternalFormat(), data);
    return tex;
}

SharedTextureRectangle TextureRectangle::createFromData(const SharedTextureData &data, GLenum internalFormat)
{
    if (!data)
    {
        error() << "TextureRectangle::createFromData failed, no data provided";
        return nullptr;
    }

    auto tex = std::make_shared<TextureRectangle>(internalFormat);
    tex->bind().setData(internalFormat, data);
    return tex;
}

bool TextureRectangle::BoundTextureRectangle::isCurrent() const
{
    GLOW_RUNTIME_ASSERT(sCurrentTexture == this, "Currently bound FBO does NOT match represented Texture", return false);
    return true;
}

void TextureRectangle::BoundTextureRectangle::makeStorageImmutable(size_t width, size_t height, GLenum internalFormat)
{
    if (!isCurrent())
        return;

    GLOW_RUNTIME_ASSERT(!texture->isStorageImmutable(), "Texture is already immutable", return );

    texture->mStorageImmutable = true;
    texture->mInternalFormat = internalFormat;
    texture->mWidth = width;
    texture->mHeight = height;
    glTexStorage2D(texture->mTarget, 1, internalFormat, width, height);
}

void TextureRectangle::BoundTextureRectangle::setMinFilter(GLenum filter)
{
    if (!isCurrent())
        return;

    auto corrected = false;
    switch (filter)
    {
    case GL_NEAREST_MIPMAP_NEAREST:
    case GL_NEAREST_MIPMAP_LINEAR:
        filter = GL_NEAREST;
        corrected = true;
        break;
    case GL_LINEAR_MIPMAP_NEAREST:
    case GL_LINEAR_MIPMAP_LINEAR:
        filter = GL_LINEAR;
        corrected = true;
        break;
    }
    if (corrected)
        warning() << "TextureRectangle does not support MipMapping.";

    glTexParameteri(texture->mTarget, GL_TEXTURE_MIN_FILTER, filter);
    texture->mMinFilter = filter;
}

void TextureRectangle::BoundTextureRectangle::setMagFilter(GLenum filter)
{
    if (!isCurrent())
        return;

    glTexParameteri(texture->mTarget, GL_TEXTURE_MAG_FILTER, filter);
    texture->mMagFilter = filter;
}

void TextureRectangle::BoundTextureRectangle::setFilter(GLenum magFilter, GLenum minFilter)
{
    if (!isCurrent())
        return;

    glTexParameteri(texture->mTarget, GL_TEXTURE_MIN_FILTER, minFilter);
    glTexParameteri(texture->mTarget, GL_TEXTURE_MAG_FILTER, magFilter);
    texture->mMinFilter = minFilter;
    texture->mMagFilter = magFilter;
}

void TextureRectangle::BoundTextureRectangle::setAnisotropicFiltering(GLfloat samples)
{
    if (!isCurrent())
        return;

    samples = glm::clamp(samples, 1.f, limits::maxAnisotropy);
    glTexParameterf(texture->mTarget, GL_TEXTURE_MAX_ANISOTROPY_EXT, samples);
    texture->mAnisotropicFiltering = samples;
}

void TextureRectangle::BoundTextureRectangle::setWrapS(GLenum wrap)
{
    if (!isCurrent())
        return;

    glTexParameteri(texture->mTarget, GL_TEXTURE_WRAP_S, wrap);
    texture->mWrapS = wrap;
}
void TextureRectangle::BoundTextureRectangle::setWrapT(GLenum wrap)
{
    if (!isCurrent())
        return;

    glTexParameteri(texture->mTarget, GL_TEXTURE_WRAP_T, wrap);
    texture->mWrapT = wrap;
}

void TextureRectangle::BoundTextureRectangle::setWrap(GLenum wrapS, GLenum wrapT)
{
    if (!isCurrent())
        return;

    glTexParameteri(texture->mTarget, GL_TEXTURE_WRAP_S, wrapS);
    texture->mWrapS = wrapS;
    glTexParameteri(texture->mTarget, GL_TEXTURE_WRAP_T, wrapT);
    texture->mWrapT = wrapT;
}


void TextureRectangle::BoundTextureRectangle::resize(size_t width, size_t height)
{
    if (!isCurrent())
        return;

    GLOW_RUNTIME_ASSERT(!texture->isStorageImmutable(), "Texture is storage immutable", return );

    texture->mWidth = width;
    texture->mHeight = height;

    GLenum format = GL_RGBA;
    switch (texture->mInternalFormat)
    {
    case GL_DEPTH_COMPONENT:
    case GL_DEPTH_COMPONENT16:
    case GL_DEPTH_COMPONENT24:
    case GL_DEPTH_COMPONENT32:
    case GL_DEPTH_COMPONENT32F:
        format = GL_DEPTH_COMPONENT;
        break;
    }

    glTexImage2D(texture->mTarget, 0, texture->mInternalFormat, width, height, 0, format, GL_FLOAT, nullptr);
}

void TextureRectangle::BoundTextureRectangle::setData(
    GLenum internalFormat, size_t width, size_t height, GLenum format, GLenum type, const GLvoid *data)
{
    if (!isCurrent())
        return;

    if (texture->isStorageImmutable())
    {
        GLOW_RUNTIME_ASSERT(texture->mWidth == width, "Texture is storage immutable and a wrong width was specified", return );
        GLOW_RUNTIME_ASSERT(texture->mHeight == height, "Texture is storage immutable and a wrong height was specified", return );
        GLOW_RUNTIME_ASSERT(texture->mInternalFormat == internalFormat,
                            "Texture is storage immutable and a wrong internal format was specified", return );
    }

    texture->mWidth = width;
    texture->mHeight = height;
    texture->mInternalFormat = internalFormat;

    // assure proper pixel store parameter
    scoped::unpackSwapBytes _p0(false);
    scoped::unpackLsbFirst _p1(false);
    scoped::unpackRowLength _p2(0);
    scoped::unpackImageHeight _p3(0);
    scoped::unpackSkipRows _p4(0);
    scoped::unpackSkipPixels _p5(0);
    scoped::unpackSkipImages _p6(0);
    scoped::unpackAlignment _p7(1); // tight

    glTexImage2D(texture->mTarget, 0, texture->mInternalFormat, width, height, 0, format, type, data);
}

void TextureRectangle::BoundTextureRectangle::setData(GLenum internalFormat, const SharedTextureData &data)
{
    if (!isCurrent())
        return;

    texture->mInternalFormat = internalFormat; // format first, then resize
    resize(data->getWidth(), data->getHeight());

    // set all level 0 surfaces
    for (auto const &surf : data->getSurfaces())
        if (surf->getMipmapLevel() == 0)
            setData(internalFormat, surf->getWidth(), surf->getHeight(), surf->getFormat(), surf->getType(),
                    surf->getData().data());

    // set parameters
    if (data->getAnisotropicFiltering() >= 1.f)
        setAnisotropicFiltering(data->getAnisotropicFiltering());
    if (data->getMinFilter() != GL_INVALID_ENUM)
        setMinFilter(data->getMinFilter());
    if (data->getMagFilter() != GL_INVALID_ENUM)
        setMagFilter(data->getMagFilter());
    if (data->getWrapS() != GL_INVALID_ENUM)
        setWrapS(data->getWrapS());
    if (data->getWrapT() != GL_INVALID_ENUM)
        setWrapT(data->getWrapT());
}

std::vector<char> TextureRectangle::BoundTextureRectangle::getData(GLenum format, GLenum type)
{
    if (!isCurrent())
        return {};


    // assure proper pixel store parameter
    scoped::packSwapBytes _p0(false);
    scoped::packLsbFirst _p1(false);
    scoped::packRowLength _p2(0);
    scoped::packImageHeight _p3(0);
    scoped::packSkipRows _p4(0);
    scoped::packSkipPixels _p5(0);
    scoped::packSkipImages _p6(0);
    scoped::packAlignment _p7(1); // tight

    std::vector<char> data;
    data.resize(texture->mWidth * texture->mHeight * channelsOfFormat(format) * sizeOfTypeInBytes(type));
    glGetTexImage(texture->mTarget, 0, format, type, data.data());
    return data;
}

void TextureRectangle::BoundTextureRectangle::getData(GLenum format, GLenum type, size_t bufferSize, void *buffer)
{
    if (!isCurrent())
        return;


    // assure proper pixel store parameter
    scoped::packSwapBytes _p0(false);
    scoped::packLsbFirst _p1(false);
    scoped::packRowLength _p2(0);
    scoped::packImageHeight _p3(0);
    scoped::packSkipRows _p4(0);
    scoped::packSkipPixels _p5(0);
    scoped::packSkipImages _p6(0);
    scoped::packAlignment _p7(1); // tight

    (void)bufferSize; // TODO: check me!
    glGetTexImage(texture->mTarget, 0, format, type, buffer);
}

SharedTextureData TextureRectangle::BoundTextureRectangle::getTextureData()
{
    if (!isCurrent())
        return nullptr;

    // assure proper pixel store parameter
    scoped::packSwapBytes _p0(false);
    scoped::packLsbFirst _p1(false);
    scoped::packRowLength _p2(0);
    scoped::packImageHeight _p3(0);
    scoped::packSkipRows _p4(0);
    scoped::packSkipPixels _p5(0);
    scoped::packSkipImages _p6(0);
    scoped::packAlignment _p7(1); // tight

    auto tex = std::make_shared<TextureData>();

    // tex parameters
    // TODO

    // surfaces
    {
        auto lvl = 0;
        GLint w;
        GLint h;
        GLint d;
        GLenum internalFormat;
        glGetTexLevelParameteriv(texture->mTarget, lvl, GL_TEXTURE_WIDTH, &w);
        glGetTexLevelParameteriv(texture->mTarget, lvl, GL_TEXTURE_HEIGHT, &h);
        glGetTexLevelParameteriv(texture->mTarget, lvl, GL_TEXTURE_DEPTH, &d);
        glGetTexLevelParameteriv(texture->mTarget, lvl, GL_TEXTURE_INTERNAL_FORMAT, (GLint *)&internalFormat);

        {
            tex->setWidth(w);
            tex->setHeight(h);
            tex->setDepth(d);
            tex->setPreferredInternalFormat(internalFormat);
        }

        auto surface = std::make_shared<SurfaceData>();
        surface->setWidth(w);
        surface->setHeight(h);
        surface->setDepth(d);

        // TODO: support all formats
        surface->setFormat(GL_RGBA);
        surface->setType(GL_UNSIGNED_BYTE);

        std::vector<char> data;
        data.resize(4 * w * h * d);
        glGetTexImage(texture->mTarget, lvl, surface->getFormat(), surface->getType(), data.data());
        surface->setData(data);

        tex->addSurface(surface);
    }

    return tex;
}

void TextureRectangle::BoundTextureRectangle::writeToFile(const std::string &filename)
{
    getTextureData()->saveToFile(filename);
}


TextureRectangle::BoundTextureRectangle::BoundTextureRectangle(TextureRectangle *texture) : texture(texture)
{
    glGetIntegerv(texture->mBindingTarget, &previousTexture);
    glActiveTexture(GL_TEXTURE0 + limits::maxCombinedTextureImageUnits - 1);
    glBindTexture(texture->mTarget, texture->mObjectName);

    previousTexturePtr = sCurrentTexture;
    sCurrentTexture = this;
}

TextureRectangle::BoundTextureRectangle::BoundTextureRectangle(TextureRectangle::BoundTextureRectangle &&rhs)
  : texture(rhs.texture), previousTexture(rhs.previousTexture), previousTexturePtr(rhs.previousTexturePtr)
{
    // invalidate rhs
    rhs.previousTexture = -1;
}

TextureRectangle::BoundTextureRectangle::~BoundTextureRectangle()
{
    if (previousTexture != -1) // if valid
    {
        glActiveTexture(GL_TEXTURE0 + limits::maxCombinedTextureImageUnits - 1);
        glBindTexture(texture->mTarget, previousTexture);
        sCurrentTexture = previousTexturePtr;
    }
}
