#include "Texture.hh"

using namespace glow;

Texture::Texture(GLenum target, GLenum bindingTarget, GLenum internalFormat)
  : mTarget(target), mBindingTarget(bindingTarget), mInternalFormat(internalFormat)
{
    glGenTextures(1, &mObjectName);

    GLint oldTex;
    glGetIntegerv(mBindingTarget, &oldTex);
    glBindTexture(mTarget, mObjectName); // pin this texture to the correct type
    glBindTexture(mTarget, oldTex);      // restore prev
}

Texture::~Texture()
{
    glDeleteTextures(1, &mObjectName);
}
