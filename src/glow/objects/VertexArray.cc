#include "VertexArray.hh"

#include "Program.hh"
#include "ElementArrayBuffer.hh"
#include "ArrayBuffer.hh"
#include "Framebuffer.hh"

#include "glow/common/runtime_assert.hh"
#include "glow/common/log.hh"
#include "glow/util/LocationMapping.hh"
#include "glow/util/UniformState.hh"

using namespace glow;

VertexArray::BoundVertexArray *VertexArray::sCurrentVAO = nullptr;

VertexArray::BoundVertexArray *VertexArray::getCurrentVAO()
{
    return sCurrentVAO;
}

VertexArray::VertexArray(GLenum primitiveMode) : mPrimitiveMode(primitiveMode)
{
    glGenVertexArrays(1, &mObjectName);

    mAttributeMapping = std::make_shared<LocationMapping>();
}

VertexArray::~VertexArray()
{
    glDeleteVertexArrays(1, &mObjectName);
}

SharedVertexArray VertexArray::create(GLenum primitiveMode)
{
    return std::make_shared<VertexArray>(primitiveMode);
}

SharedVertexArray VertexArray::create(const SharedArrayBuffer &ab, const SharedElementArrayBuffer &eab, GLenum primitiveMode)
{
    auto vao = create(primitiveMode);
    auto bvao = vao->bind();
    bvao.attach(ab);
    bvao.attach(eab);
    return vao;
}

SharedVertexArray VertexArray::create(const std::vector<SharedArrayBuffer> &abs, const SharedElementArrayBuffer &eab, GLenum primitiveMode)
{
    auto vao = create(primitiveMode);
    auto bvao = vao->bind();
    bvao.attach(abs);
    bvao.attach(eab);
    return vao;
}

bool VertexArray::getBufferForAttribute(const std::string& name, SharedArrayBuffer& result) const {
	auto index = getAttributeMapping()->queryLocation(name);
	if (index == -1) {
        return false;
	}

    if (index >= mAttributes.size()) {
        return false;
    }

    result = (SharedArrayBuffer)mAttributes[index].buffer;
    return true;
}

SharedElementArrayBuffer const & glow::VertexArray::getIdxBuffer() const {
    return mElementArrayBuffer;
}

void VertexArray::BoundVertexArray::draw(GLsizei instanceCount)
{
    if (!isCurrent())
        return;

    negotiateBindings();

    if (vao->mElementArrayBuffer)
    {
        // draw indexed
        glDrawElementsInstanced(vao->mPrimitiveMode, vao->mElementArrayBuffer->getIndexCount(),
                                vao->mElementArrayBuffer->getIndexType(), nullptr, instanceCount);
    }
    else
    {
        // vertex count is first divisor-0 attribute
        auto vCount = 0u;
        for (auto const &a : vao->mAttributes)
            if (a.buffer->getAttributes()[a.locationInBuffer].divisor == 0)
            {
                vCount = a.buffer->getElementCount();
                break;
            }

        // draw unindexed
        glDrawArraysInstanced(vao->mPrimitiveMode, 0, vCount, instanceCount);
    }
}

void VertexArray::BoundVertexArray::drawRange(GLsizei start, GLsizei end, GLsizei instanceCount)
{
    if (!isCurrent())
        return;

    negotiateBindings();

    if (vao->mElementArrayBuffer)
    {
        // draw indexed
        auto indexSize = ptrdiff_t{0};
        switch (vao->mElementArrayBuffer->getIndexType())
        {
        case GL_UNSIGNED_BYTE:
            indexSize = 1;
            break;
        case GL_UNSIGNED_SHORT:
            indexSize = 2;
            break;
        case GL_UNSIGNED_INT:
            indexSize = 4;
            break;

        default:
            error() << "Unknown index type: " << vao->mElementArrayBuffer->getIndexType();
            return;
        }

        glDrawElementsInstanced(vao->mPrimitiveMode, end - start, vao->mElementArrayBuffer->getIndexType(),
                                (void *)(start * indexSize), instanceCount);
    }
    else
    {
        // draw unindexed
        glDrawArraysInstanced(vao->mPrimitiveMode, start, end - start, instanceCount);
    }
}

void VertexArray::BoundVertexArray::attach(const SharedElementArrayBuffer &eab)
{
    if (!isCurrent())
        return;

    vao->mElementArrayBuffer = eab;
    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, eab ? eab->getObjectName() : 0);
}

void VertexArray::attachAttribute(const VertexArrayAttribute &va)
{
    auto boundAB = va.buffer->bind(); // important, buffer needs to be bound.

    auto const &a = va.buffer->getAttributes()[va.locationInBuffer];
    auto stride = va.buffer->getStride();
    glEnableVertexAttribArray(va.locationInVAO);
    switch (a.mode)
    {
    case AttributeMode::Float:
        glVertexAttribPointer(va.locationInVAO, a.size, a.type, GL_FALSE, stride, reinterpret_cast<const GLvoid *>(a.offset));
        break;
    case AttributeMode::NormalizedInteger:
        glVertexAttribPointer(va.locationInVAO, a.size, a.type, GL_TRUE, stride, reinterpret_cast<const GLvoid *>(a.offset));
        break;
    case AttributeMode::Integer:
        glVertexAttribIPointer(va.locationInVAO, a.size, a.type, stride, reinterpret_cast<const GLvoid *>(a.offset));
        break;
    case AttributeMode::Double:
#if GLOW_OPENGL_VERSION >= 41
        glVertexAttribLPointer(va.locationInVAO, a.size, a.type, stride, reinterpret_cast<const GLvoid *>(a.offset));
#else
        warning() << "Real 64bit precision double attributes are only supported in OpenGL 4.1+";
#endif
        break;

    default:
        assert(0 && "not implemented");
    }
}

void VertexArray::BoundVertexArray::attach(const SharedArrayBuffer &ab)
{
    if (!isCurrent())
        return;

    auto const &attrs = ab->getAttributes();
    for (auto i = 0u; i < attrs.size(); ++i)
    {
        auto const &a = attrs[i];
        auto loc = vao->mAttributeMapping->getOrAddLocation(a.name);

        VertexArrayAttribute va = {ab, i, loc};
        vao->mAttributes.push_back(va);

        attachAttribute(va);
    }
}

void VertexArray::BoundVertexArray::attach(const std::vector<SharedArrayBuffer> &abs)
{
    if (!isCurrent())
        return;

    for (auto const &ab : abs)
        attach(ab);
}

void VertexArray::BoundVertexArray::reattach()
{
    int attribs = 0;
    glGetIntegerv(GL_MAX_VERTEX_ATTRIBS, &attribs);

    // disable all
    for (auto loc = 0; loc < attribs; ++loc)
        glDisableVertexAttribArray(loc);

    // update mappings
    for (auto &va : vao->mAttributes)
    {
        auto loc = vao->mAttributeMapping->queryLocation(va.buffer->getAttributes()[va.locationInBuffer].name);
        assert(loc >= 0 && "mapping should be present!");
        va.locationInVAO = loc;
    }

    // attach newly mapped
    for (auto const &va : vao->mAttributes)
        attachAttribute(va);
}

void VertexArray::BoundVertexArray::negotiateBindings()
{
    // negotiate mappings on the fly
    auto currProg = Program::getCurrentProgram();
    if (currProg)
    {
        bool changedVAO = false;
        bool changedProgramA = false;
        bool changedProgramB = false;
        bool changedFBO = false;
        LocationMapping::negotiate(vao->mAttributeMapping, currProg->program->mAttributeMapping, changedVAO, changedProgramA);

        auto fbo = Framebuffer::getCurrentBuffer();
        if (fbo)
            LocationMapping::negotiate(fbo->buffer->mFragmentMapping, currProg->program->mFragmentMapping, changedFBO,
                                       changedProgramB, true);

        if (changedProgramA || changedProgramB || changedFBO /* due to lack of frag API */)
        {
            if (changedProgramB || changedFBO)
                currProg->program->resetFragmentLocationCheck(); /* due to lack of frag API */

            // save uniforms
            auto uniforms = currProg->program->getUniforms();
            // relink
            currProg->program->link();
            // restore (program is relinked)
            uniforms->restore();
        }

        if (changedVAO)
        {
            // reattach attributes
            reattach();
        }

        if (changedFBO)
        {
            assert(fbo);
            // reattach fbo
            fbo->reattach();
        }

        // check fbo
        if (changedFBO || changedProgramB)
        {
            assert(fbo);
            fbo->checkComplete();
        }

        // check vao
        if (changedVAO || changedProgramA)
        {
            auto shaderLocs = currProg->program->extractAttributeLocations();
            for (auto const &kvp : shaderLocs)
            {
                auto found = false;
                for (auto const &va : vao->mAttributes)
                    if (va.buffer->getAttributes()[va.locationInBuffer].name == kvp.first)
                    {
                        found = true;
                        break;
                    }

                if (!found)
                {
                    error() << "Shader requires attribute '" << kvp.first
                            << "' but VAO does not define this attribute!";
                }
            }
        }
    }
}

VertexArray::BoundVertexArray::BoundVertexArray(VertexArray *vao) : vao(vao)
{
    GLOW_RUNTIME_ASSERT(ElementArrayBuffer::getCurrentBuffer() == nullptr,
                        "Cannot bind a VAO while an EAB is bound! (this has unintended side-effects in OpenGL)", return );

    glGetIntegerv(GL_VERTEX_ARRAY_BINDING, &previousVAO);
    glGetIntegerv(GL_ELEMENT_ARRAY_BUFFER_BINDING, &previousEAB);
    glBindVertexArray(vao->mObjectName);

    previousVaoPtr = sCurrentVAO;
    sCurrentVAO = this;
}

bool VertexArray::BoundVertexArray::isCurrent() const
{
    GLOW_RUNTIME_ASSERT(sCurrentVAO == this, "Currently bound VAO does NOT match represented vao", return false);
    return true;
}

VertexArray::BoundVertexArray::BoundVertexArray(VertexArray::BoundVertexArray &&rhs)
  : vao(rhs.vao), previousVAO(rhs.previousVAO), previousEAB(rhs.previousEAB), previousVaoPtr(rhs.previousVaoPtr)
{
    // invalidate rhs
    rhs.previousVAO = -1;
}

VertexArray::BoundVertexArray::~BoundVertexArray()
{
    if (previousVAO != -1) // if valid
    {
        glBindVertexArray(previousVAO);
        glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, previousEAB);
        sCurrentVAO = previousVaoPtr;
    }
}
