#pragma once

#include "glow/common/shared.hh"
#include "glow/common/non_copyable.hh"
#include "glow/common/property.hh"

#include "glow/gl.hh"

#include <vector>

namespace glow
{
/**
 * @brief Container for a single continuous data layer of a texture
 *
 * E.g. cube maps have 6 surfaces at mipmap level 0
 */
GLOW_SHARED(class, SurfaceData);
class SurfaceData
{
    GLOW_NON_COPYABLE(SurfaceData);

private:
    int mMipmapLevel = 0;
    int mLayer = 0;

    size_t mWidth = 1;
    size_t mHeight = 1;
    size_t mDepth = 1;

    GLenum mFormat;
    GLenum mType;

    /// special target for cubemaps
    GLenum mTarget = GL_INVALID_ENUM;

    std::vector<char> mData;

public: // getter, setter
    GLOW_PROPERTY(Width);
    GLOW_PROPERTY(Height);
    GLOW_PROPERTY(Depth);

    size_t getSize() const { return mWidth * mHeight * mDepth; }
    GLOW_PROPERTY(Format);
    GLOW_PROPERTY(Type);

    GLOW_PROPERTY(Target);

    GLOW_PROPERTY(MipmapLevel);
    GLOW_PROPERTY(Layer);

    GLOW_PROPERTY(Data);

public:
    SurfaceData();
};
}
