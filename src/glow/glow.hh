#pragma once

namespace glow
{
/// OpenGL Version information
/// Only valid after initGLOW
extern struct _glowGLVersion
{
    int major;
    int minor;
    int total; // like 43 for 4.3
} OGLVersion;

/**
 * @brief initializes the GLOW Library
 *
 * In particular:
 *   - loads OpenGL function
 *   - performs certain static initialization
 *
 * @return true on success
 */
bool initGLOW();
}
