#include "glow.hh"
#include "gl.hh"
#include "debug.hh"
#include "limits.hh"

#include "common/log.hh"

#ifdef GLOW_ACGL_COMPAT
#include <ACGL/OpenGL/Tools.hh>
#endif

using namespace glow;

glow::_glowGLVersion glow::OGLVersion;

bool glow::initGLOW()
{
#ifdef GLOW_ACGL_COMPAT
    // assume already initialized
    OGLVersion.major = ACGL::OpenGL::getOpenGLMajorVersionNumber();
    OGLVersion.minor = ACGL::OpenGL::getOpenGLMinorVersionNumber();
#else
    if (!gladLoadGL())
        return false;

    OGLVersion.major = GLVersion.major;
    OGLVersion.minor = GLVersion.minor;
#endif
    OGLVersion.total = OGLVersion.major * 10 + OGLVersion.minor;
    info() << "Loaded OpenGL Version " << OGLVersion.major << "." << OGLVersion.minor;

    // install debug message
    if (glDebugMessageCallback)
    {
        glEnable(GL_DEBUG_OUTPUT);
        glDebugMessageCallback(DebugMessageCallback, nullptr);
    }

    // update limits
    limits::update();

    return true;
}
